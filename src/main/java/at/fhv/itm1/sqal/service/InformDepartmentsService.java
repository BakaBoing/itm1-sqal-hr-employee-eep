package at.fhv.itm1.sqal.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InformDepartmentsService implements JavaDelegate {

    private static final String DEP_NAME_VAR = "depName";
    private static final String DEP_INFO_MAIL_VAR = "depInfoMailAddress";
    private static final String DEP_MAIL_SENT_VAR = "depInfoSent";
    private static final String EMAIL_REGEX = "^(.+)@(.+)$";

    private final Logger LOGGER = Logger.getLogger(InformDepartmentsService.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String name = (String) delegateExecution.getVariable(DEP_NAME_VAR);
        String email = (String) delegateExecution.getVariable(DEP_INFO_MAIL_VAR);

        boolean sent = informDepartmentOnEmployeeLeave(name, email);
        delegateExecution.setVariable(DEP_MAIL_SENT_VAR, sent);
    }

    protected boolean informDepartmentOnEmployeeLeave(String name, String email) {
        boolean sent;
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);

        if (matcher.matches()) {
            /* send mail here*/
            LOGGER.info("Department \"" + name + "\" was informed on \"" + email + "\" of employee leave.");
            sent = true;
        } else {
            LOGGER.info("Department \"" + name + "\" could not be informed.");
            sent = false;
        }
        return sent;
    }
}
