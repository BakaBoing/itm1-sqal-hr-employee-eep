package at.fhv.itm1.sqal.service;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class MailFeedbackService implements JavaDelegate {
    private final static Map<String, String> _mailMock = new HashMap<String, String>() {{
        put("HRMember", "peter.lustig@fhv.at");
        put("Teammember", "gustav.gans@fhv.at");
        put("Teamleader", "sandra.tempo@fhv.at");
        put("Departmentmember", "donald.duck@fhv.at");
        put("Departmentleader", "maxime.musterperson@fhv.at");
    }};

    private final Logger LOGGER = Logger.getLogger(InformDepartmentsService.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        inviteToFeedback((String) delegateExecution.getVariable("invitedPeople"));
    }

    protected HashMap<String, String> inviteToFeedback(String invited) {
        HashMap<String, String> actualInvited = new HashMap<>();

        Arrays.stream(invited.split(",")).forEach((String role) -> {
            role = role.trim();
            String email = _mailMock.get(role);
            // send mail here
            if(email != null) {
                actualInvited.put(role, email);
            }
        });

        return actualInvited;
    }

}
