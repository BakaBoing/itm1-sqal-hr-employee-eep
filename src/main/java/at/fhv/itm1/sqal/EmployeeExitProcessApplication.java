package at.fhv.itm1.sqal;

import org.apache.catalina.webresources.TomcatURLStreamHandlerFactory;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableProcessApplication
public class EmployeeExitProcessApplication {
    @Autowired
    private ProcessEngine processEngine;

    public static void main(String... args) {
        // Avoid resetting URL stream handler factory
        TomcatURLStreamHandlerFactory.disable();
        SpringApplication.run(EmployeeExitProcessApplication.class, args);
    }

    @PostConstruct
    public void deployEmployeeExit() {
        ClassLoader classLoader = EmployeeExitProcessApplication.class.getClassLoader();

        if (processEngine.getIdentityService().createUserQuery().list().isEmpty()) {
            processEngine.getRepositoryService()
                    .createDeployment()
                    .addInputStream("employee_exit_process.bpmn", classLoader.getResourceAsStream("employee_exit_process.bpmn"))
                    .addInputStream("check_contract_violations_process.bpmn", classLoader.getResourceAsStream("check_contract_violations_process.bpmn"))
                    .addInputStream("inform_departments_process.bpmn", classLoader.getResourceAsStream("inform_departments_process.bpmn"))
                    .addInputStream("invite_decision_matrix.dmn", classLoader.getResourceAsStream("invite_decision_matrix.dmn"))
                    .deploy();
        }
    }
}
