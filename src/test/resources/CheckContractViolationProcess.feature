Feature: Check Contract Violation

    Test the check contract violation subprocess

    Scenario: Check valid contract notice period and no debt
        Given Check contract violations subprocess started
        When Check violation of notice period that 'is not violated'
        And There 'is no outstanding' debt to the company
        Then Violation 'does not exist'

    Scenario: Check invalid contract notice period
        Given Check contract violations subprocess started
        When Check violation of notice period that 'is violated'
        Then Violation 'exists'

    Scenario: Check valid contract notice period but outstanding debt
        Given Check contract violations subprocess started
        When Check violation of notice period that 'is not violated'
        And There 'is an outstanding' debt to the company
        Then Debt 'exists'
