package at.fhv.itm1.sqal.service;

import org.junit.Test;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MailFeedbackServiceTest {

    @Test
    public void testInviteFeedHRMember() {
        MailFeedbackService service = new MailFeedbackService();
        String role = "HRMember";
        HashMap<String, String> invited = service.inviteToFeedback(role);
        assertEquals("peter.lustig@fhv.at", invited.get(role));
    }

    @Test
    public void testInviteFeedTeammember() {
        MailFeedbackService service = new MailFeedbackService();
        String role = "Teammember";
        HashMap<String, String> invited = service.inviteToFeedback(role);
        assertEquals("gustav.gans@fhv.at", invited.get(role));
    }

    @Test
    public void testInviteFeedTeamleader() {
        MailFeedbackService service = new MailFeedbackService();
        String role = "Teamleader";
        HashMap<String, String> invited = service.inviteToFeedback(role);
        assertEquals("sandra.tempo@fhv.at", invited.get(role));
    }

    @Test
    public void testInviteFeedDepartmentMember() {
        MailFeedbackService service = new MailFeedbackService();
        String role = "Departmentmember";
        HashMap<String, String> invited = service.inviteToFeedback(role);
        assertEquals("donald.duck@fhv.at", invited.get(role));
    }

    @Test
    public void testInviteFeedDepartmentLeader() {
        MailFeedbackService service = new MailFeedbackService();
        String role = "Departmentleader";
        HashMap<String, String> invited = service.inviteToFeedback(role);
        assertEquals("maxime.musterperson@fhv.at", invited.get(role));
    }

    @Test
    public void testInviteNone(){
        MailFeedbackService service = new MailFeedbackService();
        HashMap<String, String> invited = service.inviteToFeedback("");
        assertEquals(0, invited.size());
    }

    @Test
    public void testInviteInvalidRole(){
        MailFeedbackService service = new MailFeedbackService();
        HashMap<String, String> invited = service.inviteToFeedback("DönerDaniel");
        assertEquals(0, invited.size());
    }
}
