package at.fhv.itm1.sqal.service;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InformDepartmentsServiceTest {
    @Test
    public void testEmailSentSuccess() {
        InformDepartmentsService service = new InformDepartmentsService();
        boolean sent = service.informDepartmentOnEmployeeLeave("TestDepartment", "test@dep.at");
        assertTrue(sent);
    }

    @Test
    public void testEmailSentFail() {
        InformDepartmentsService service = new InformDepartmentsService();
        boolean sent = service.informDepartmentOnEmployeeLeave("TestDepartment", "testdep.at");
        assertFalse(sent);
    }
}
