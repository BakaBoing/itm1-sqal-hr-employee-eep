package at.fhv.itm1.sqal.bdd;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages ="at.fhv.itm1.sqal")
class TestConfiguration {
// defines a webdriver Bean
}
