package at.fhv.itm1.sqal.bdd;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        properties = {
                "camunda.bpm.generate-unique-process-engine-name=true",
                "camunda.bpm.generate-unique-process-application-name=true",
                "spring.datasource.generate-unique-name=true",
        }
)
@CucumberContextConfiguration
@ContextConfiguration(classes = TestConfiguration.class)
public class CheckContractViolationProcessStepDefs {
    @Autowired
    ProcessEngine processEngine;

    @Autowired
    RuntimeService runtimeService;
    ProcessInstance instance;

    @Before
    public void setUp() {
        init(this.processEngine);
    }

    @After
    public void tearDown() {
        System.out.println("Stopping a CheckContractViolationProcess scenario");
    }

    @Given("Check contract violations subprocess started")
    public void check_contract_violations_subprocess_started() {
        this.instance = this.runtimeService.startProcessInstanceByKey("CheckContractViolationsProcess");
        assertThat(this.instance).isNotNull();
        assertThat(this.instance).isStarted();
    }

    @When("Check violation of notice period that {string}")
    public void check_violation_of_notice_period(String violationText) {
        boolean violated = violationText.equals("is violated");
        complete(task(this.instance), withVariables("violated", violated));
    }

    @And("There {string} debt to the company")
    public void there_is_no_outstanding_debt_to_the_company(String debtText) {
        boolean noDebtExists = debtText.equals("is no outstanding");
        complete(task(this.instance), withVariables("noDebtExists", noDebtExists));

        if (!noDebtExists) {
            assertThat(this.instance).hasPassed("startRepaymentProcess");
        }
    }

    @Then("Violation {string}")
    public void violation_non_existent(String violationText) {
        boolean violated = violationText.equals("exists");

        if (violated) {
            assertThat(this.instance)
                    .hasPassed("contract_violated")
                    .isEnded();
        } else {
            assertThat(this.instance)
                    .hasPassed("contractNotViolated")
                    .isEnded();
        }
    }

    @Then("Debt {string}")
    public void debt_exists(String debtText) {
        boolean debtExists = debtText.equals("exists");

        if (debtExists) {
            assertThat(this.instance).hasPassed("startRepaymentProcess").isEnded();
        } else {
            assertThat(this.instance)
                    .hasPassed("checkDebtToCompany")
                    .hasPassed("contractNotViolated")
                    .isEnded();
        }
    }
}
