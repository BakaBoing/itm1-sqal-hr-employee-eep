package at.fhv.itm1.sqal;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@SpringBootTest(
        properties = {
                "camunda.bpm.generate-unique-process-engine-name=true",
                "camunda.bpm.generate-unique-process-application-name=true",
                "spring.datasource.generate-unique-name=true",
        }
)
public class EmployeeExitProcessIntegrationTest {
    @Autowired
    ProcessEngine processEngine;

    @Autowired
    RuntimeService runtimeService;

    @BeforeEach
    public void setUp() {
        init(this.processEngine);
    }

    @Test
    public void make_sure_subprocess_gets_integrated() {
        ProcessInstance process = this.runtimeService.startProcessInstanceByKey("EmployeeExitProcess");
        assertThat(process).isNotNull();
        assertThat(process).isStarted();
    }

    @Test
    public void subprocess_check_contract_violation_integrated() {
        ProcessInstance process = this.runtimeService.startProcessInstanceByKey("EmployeeExitProcess");
        assertThat(process).isNotNull();
        assertThat(process).isStarted();
        assertThat(process).isWaitingAt("checkContractViolations");

        ProcessInstance subprocess = processEngine.getRuntimeService().createProcessInstanceQuery().superProcessInstanceId(process.getId()).singleResult();
        complete(task(subprocess), withVariables("violated", false));
        complete(task(subprocess), withVariables("noDebtExists", true));
        assertThat(subprocess).hasPassed("checkDebtToCompany");

        assertThat(process).hasPassed("checkContractViolations");
    }

    @Test
    public void subprocess_inform_departments_integrated() {
        ProcessInstance process = this.runtimeService.startProcessInstanceByKey("EmployeeExitProcess");
        assertThat(process).isNotNull();
        assertThat(process).isStarted();
        assertThat(process).isWaitingAt("checkContractViolations");

        ProcessInstance subprocess = processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .superProcessInstanceId(process.getId())
                .singleResult();
        complete(task(subprocess), withVariables("violated", false));
        complete(task(subprocess), withVariables("noDebtExists", true));
        assertThat(subprocess).hasPassed("checkDebtToCompany");

        assertThat(process).hasPassed("checkContractViolations");
        assertThat(process).hasPassed("informDepartments");
    }

    @Test
    public void all_integrated() {
        ProcessInstance process = this.runtimeService.startProcessInstanceByKey("EmployeeExitProcess");
        assertThat(process).isNotNull();
        assertThat(process).isStarted();
        assertThat(process).isWaitingAt("checkContractViolations");

        ProcessInstance subprocess1 = processEngine.getRuntimeService()
                .createProcessInstanceQuery()
                .superProcessInstanceId(process.getId())
                .singleResult();
        complete(task(subprocess1), withVariables("violated", false));
        complete(task(subprocess1), withVariables("noDebtExists", true));
        assertThat(subprocess1).hasPassed("checkDebtToCompany");

        assertThat(process).hasPassed("checkContractViolations");
        assertThat(process).hasPassed("informDepartments");
        assertThat(process).isWaitingAt("prepareFinalPaperwork");
        complete(task(process), withVariables("employeeRole", "val0"));
        assertThat(process).isEnded();
    }
}
